// Package fsm provides a basic finite state machine
package fsm

// StateMachine represents a state machine with states S and inputs I
type StateMachine[S comparable, I comparable] struct {
	initialState S
	currentState S

	States map[S]map[I]S
}

// NewStateMachine creates a new state machine with states S and inputs I
func NewStateMachine[S comparable, I comparable](initialState S, States map[S]map[I]S) *StateMachine[S, I] {
	return &StateMachine[S, I]{
		initialState: initialState,
		currentState: initialState,
		States:       States,
	}
}

// CurrentState returns the current state of the state machine
//
// returns S
func (sm *StateMachine[S, I]) CurrentState() S {
	return sm.currentState
}

// Transition moves the state machine from the current state to the new state if the current state has input I
//
// input -> takes the input verb
func (sm *StateMachine[S, I]) Transition(input I) {
	if currentTransitions, ok := sm.States[sm.currentState]; ok {
		if dest, tOk := currentTransitions[input]; tOk {
			sm.currentState = dest
		}
	}
}

// AllowTransition checks to see if the currentState has a transition identified by input
//
// input -> takes the input verb
//
// returns bool
func (sm *StateMachine[S, I]) AllowTransition(input I) bool {
	if currentTransitions, ok := sm.States[sm.currentState]; ok {
		_, tOK := currentTransitions[input]
		return tOK
	}
	return false
}

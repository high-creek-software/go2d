package debug

import (
	"image/color"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/vector"
	vec "github.com/quartercastle/vector"
)

// DrawRect draws a rectangle on the screen, with regards of an X and Y offset as a camera projection. At the given point, as well as width, height and color.
func DrawRect(screen *ebiten.Image, xOffset, yOffset float64, leftVec vec.Vector, width, height float64, outlineColor color.Color) {
	vector.StrokeLine(screen,
		float32(leftVec.X()+xOffset),
		float32(leftVec.Y()+yOffset),
		float32(leftVec.X()+xOffset+width),
		float32(leftVec.Y()+yOffset),
		1,
		outlineColor,
		true,
	)

	vector.StrokeLine(screen,
		float32(leftVec.X()+xOffset),
		float32(leftVec.Y()+yOffset),
		float32(leftVec.X()+xOffset),
		float32(leftVec.Y()+yOffset+height),
		1,
		outlineColor,
		true,
	)

	vector.StrokeLine(screen,
		float32(leftVec.X()+xOffset),
		float32(leftVec.Y()+yOffset+height),
		float32(leftVec.X()+xOffset+width),
		float32(leftVec.Y()+yOffset+height),
		1,
		outlineColor,
		true,
	)

	vector.StrokeLine(screen,
		float32(leftVec.X()+xOffset+width),
		float32(leftVec.Y()+yOffset),
		float32(leftVec.X()+xOffset+width),
		float32(leftVec.Y()+yOffset+height),
		1,
		outlineColor,
		true,
	)
}

package debug

import (
	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/vector"
	"gitlab.com/high-creek-software/go2d/components/subjects"
	"golang.org/x/image/colornames"
	"image/color"
)

// OriginOpt is used to configure the OriginDrawComponent
type OriginOpt func(oc *OriginDrawComponent)

// OriginDrawComponent draws a point at the entity origin
type OriginDrawComponent struct {
	origin    subjects.Originer
	color     color.Color
	radius    float32
	antialias bool
}

// NewOriginDrawComponent creates a new OriginDrawComponent at origin with configuring options
func NewOriginDrawComponent(origin subjects.Originer, opts ...OriginOpt) *OriginDrawComponent {
	oc := &OriginDrawComponent{
		origin: origin,
		color:  colornames.Red,
		radius: 3,
	}

	for _, opt := range opts {
		opt(oc)
	}

	return oc
}

// Update to be called via the main game loop. Unused at this point.
func (oc *OriginDrawComponent) Update() error {
	return nil
}

// Draw draws the point on the screen with the given X and Y offsets.
// These offsets are used for a camera projection.
func (oc *OriginDrawComponent) Draw(screen *ebiten.Image, xOffset, yOffset float64) {
	vector.DrawFilledCircle(
		screen,
		float32(oc.origin.At().X()+xOffset),
		float32(oc.origin.At().Y()+yOffset),
		oc.radius,
		oc.color,
		oc.antialias,
	)
}

// SetColor is the fluent api for setting the color
func (oc *OriginDrawComponent) SetColor(c color.Color) *OriginDrawComponent {
	oc.color = c
	return oc
}

// SetRadius is the fluent api for setting the radius
func (oc *OriginDrawComponent) SetRadius(radius float32) *OriginDrawComponent {
	oc.radius = radius
	return oc
}

// SetAntiAlias is the fluent api for setting the antialias property
func (oc *OriginDrawComponent) SetAntiAlias() *OriginDrawComponent {
	oc.antialias = true
	return oc
}

// SetColor is an OriginOpt for setting the color of the point
func SetColor(c color.Color) OriginOpt {
	return func(oc *OriginDrawComponent) {
		oc.color = c
	}
}

// SetRadius is an OriginOpt for setting the radius of the point
func SetRadius(radius float32) OriginOpt {
	return func(oc *OriginDrawComponent) {
		oc.radius = radius
	}
}

// SetAntiAlias is an OriginOpt for setting the anti aliasing nature of the point
func SetAntiAlias() OriginOpt {
	return func(oc *OriginDrawComponent) {
		oc.antialias = true
	}
}

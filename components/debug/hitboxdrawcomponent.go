package debug

import (
	"image/color"

	"github.com/hajimehoshi/ebiten/v2"
	vec "github.com/quartercastle/vector"
	"gitlab.com/high-creek-software/go2d/components/subjects"
	"golang.org/x/image/colornames"
)

// HitBoxDrawOpt is used to configure the HitBoxDrawComponent
type HitBoxDrawOpt func(hb *HitBoxDrawComponent)

// HitBoxDrawComponent is used to draw the hit box of a given entity
type HitBoxDrawComponent struct {
	origin         subjects.Originer
	originAsCenter bool
	width, height  float64
	outlineColor   color.Color
}

// NewHitBoxDrawComponent creates a new HitBoxDrawComponent at origin, with given width and height and specifying if the origin is centered
func NewHitBoxDrawComponent(origin subjects.Originer, originAsCenter bool, width, height float64, opts ...HitBoxDrawOpt) *HitBoxDrawComponent {
	hb := &HitBoxDrawComponent{
		origin:         origin,
		originAsCenter: originAsCenter,
		width:          width,
		height:         height,
		outlineColor:   colornames.Red,
	}

	for _, opt := range opts {
		opt(hb)
	}

	return hb
}

// Update as called via the main game loop. Unused at this point.
func (hb *HitBoxDrawComponent) Update() error {
	return nil
}

// Draw draws the hit box of the entity. X and Y offset are for a camera projection.
func (hb *HitBoxDrawComponent) Draw(screen *ebiten.Image, xOffset, yOffset float64) {
	leftVec := hb.origin.At()
	if hb.originAsCenter {
		leftVec = vec.Vector{leftVec.X() - hb.width/2, leftVec.Y() - hb.height/2}
	}

	DrawRect(screen, xOffset, yOffset, leftVec, hb.width, hb.height, hb.outlineColor)
}

// SetColor is the fluent api for setting the outline drawing color
func (hb *HitBoxDrawComponent) SetColor(c color.Color) *HitBoxDrawComponent {
	hb.outlineColor = c
	return hb
}

// HitBoxDrawColor is HitBoxDrawOpt to change the color of the hit box outline.
func HitBoxDrawColor(c color.Color) HitBoxDrawOpt {
	return func(hb *HitBoxDrawComponent) {
		hb.outlineColor = c
	}
}

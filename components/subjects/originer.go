package subjects

import "github.com/quartercastle/vector"

// Originer is to identify the origin point of an entity
type Originer interface {
	At() vector.Vector
}

// Package subjects represents of set of properties needed for the different components to interact with the game loop, not needing a concrete knowledge of the underlying type.
package subjects

// Animateable is used for animating sets of images. These methods help work through the different sizes and directions that the entity and images may take.
type Animateable interface {
	Originer
	Scale() (float64, float64)
	IsHorizontalFlipped() bool
	IsVerticalFlipped() bool
	DrawOffset() (float64, float64)
}

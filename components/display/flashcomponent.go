package display

import (
	"image/color"
	"time"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/colorm"
	"gitlab.com/high-creek-software/go2d/components/subjects"
)

type FlashComponent struct {
	subject              subjects.Animateable
	sprite               *ebiten.Image
	frameRate            float64
	totalFlashes         int
	ceneterImageOnOrigin bool
	loop                 bool
	complete             func()
	flashColor           color.Color
	isShowingFlash       bool
	opts                 *ebiten.DrawImageOptions

	frameInterval     float64
	currentIdx        int
	lastDisplayChange time.Time
}

func NewFlashComponent(subject subjects.Animateable, frameRate float64, sprite *ebiten.Image, totalFlashes int, flashColor color.Color) *FlashComponent {
	fc := &FlashComponent{
		subject:       subject,
		frameRate:     frameRate,
		sprite:        sprite,
		totalFlashes:  totalFlashes,
		flashColor:    flashColor,
		loop:          true,
		frameInterval: float64(time.Second) / frameRate,
		opts:          &ebiten.DrawImageOptions{},
	}

	return fc
}

func (fc *FlashComponent) DisableLoop(complete func()) *FlashComponent {
	fc.loop = false
	fc.complete = complete

	return fc
}

func (fc *FlashComponent) CenterImage() *FlashComponent {
	fc.ceneterImageOnOrigin = true

	return fc
}

func (fc *FlashComponent) Reset() {
	fc.currentIdx = 0
	fc.lastDisplayChange = time.Now()
}

func (fc *FlashComponent) Update() error {
	if time.Since(fc.lastDisplayChange) >= time.Duration(fc.frameInterval) {
		fc.currentIdx += 1
		fc.currentIdx = fc.currentIdx % fc.totalFlashes
		if fc.currentIdx == 0 && !fc.loop {
			fc.complete()
		}
		fc.isShowingFlash = !fc.isShowingFlash
		fc.lastDisplayChange = time.Now()
	}

	return nil
}

func (fc *FlashComponent) Draw(screen *ebiten.Image, xOffset, yOffset float64) {

	if !fc.isShowingFlash {
		fc.drawSprite(screen, xOffset, yOffset)
	} else {
		fc.drawFlash(screen, xOffset, yOffset)
	}

}

func (fc *FlashComponent) drawSprite(screen *ebiten.Image, xOffset, yOffset float64) {
	fc.opts.GeoM.Reset()

	if fc.ceneterImageOnOrigin {
		width := fc.sprite.Bounds().Dx()
		height := fc.sprite.Bounds().Dy()

		fc.opts.GeoM.Translate(-float64(width/2), -float64(height/2))
	}

	fc.opts.GeoM.Scale(fc.subject.Scale())

	if fc.subject.IsHorizontalFlipped() {
		fc.opts.GeoM.Scale(-1, 1)
	}

	if fc.subject.IsVerticalFlipped() {
		fc.opts.GeoM.Scale(1, -1)
	}

	drawXOffset, drawYOffset := fc.subject.DrawOffset()
	fc.opts.GeoM.Translate(fc.subject.At().X()+xOffset+drawXOffset, fc.subject.At().Y()+yOffset+drawYOffset)

	screen.DrawImage(fc.sprite, fc.opts)
}

func (fc *FlashComponent) drawFlash(screen *ebiten.Image, xOffset, yOffset float64) {
	opts := &colorm.DrawImageOptions{}

	opts.GeoM.Reset()

	if fc.ceneterImageOnOrigin {
		width := fc.sprite.Bounds().Dx()
		height := fc.sprite.Bounds().Dy()

		opts.GeoM.Translate(-float64(width/2), -float64(height/2))
	}

	opts.GeoM.Scale(fc.subject.Scale())

	if fc.subject.IsHorizontalFlipped() {
		opts.GeoM.Scale(-1, 1)
	}

	if fc.subject.IsVerticalFlipped() {
		opts.GeoM.Scale(1, -1)
	}
	drawXOffset, drawYOffset := fc.subject.DrawOffset()
	opts.GeoM.Translate(fc.subject.At().X()+xOffset+drawXOffset, fc.subject.At().Y()+yOffset+drawYOffset)

	// Example can be found here: https://ebitengine.org/en/examples/flood.html#Code
	var cm colorm.ColorM
	cm.Scale(0, 0, 0, 1)

	ir, ig, ib, _ := fc.flashColor.RGBA()
	r := float64(ir) / 0xff
	g := float64(ig) / 0xff
	b := float64(ib) / 0xff
	cm.Translate(r, g, b, 0)

	// cm.Translate(0, 0, 0, 0)
	colorm.DrawImage(screen, fc.sprite, cm, opts)
}

// Package display is a set of components to interact with the representation of a entity or it's sprite on screen.
package display

import (
	"time"

	"github.com/hajimehoshi/ebiten/v2"
	"gitlab.com/high-creek-software/go2d/components/subjects"
)

// AnimateOpt is a func for modifying an animate component
type AnimateOpt func(ac *AnimateComponent)

// AnimateComponent is the core way of animating a sprite on screen
type AnimateComponent struct {
	subject             subjects.Animateable
	sprites             []*ebiten.Image
	frameRate           float64
	centerImageOnOrigin bool
	loop                bool
	complete            func()

	frameInterval     float64
	currentIdx        int
	lastDisplayChange time.Time
}

// NewAnimateComponent creates a new AnimateComponent, with reference to the subject
func NewAnimateComponent(subject subjects.Animateable, frameRate float64, sprites []*ebiten.Image, opts ...AnimateOpt) *AnimateComponent {
	ac := &AnimateComponent{
		subject:       subject,
		frameRate:     frameRate,
		sprites:       sprites,
		loop:          true,
		frameInterval: float64(time.Second) / frameRate,
	}

	for _, opt := range opts {
		opt(ac)
	}

	return ac
}

// Reset is used to return the current frame index to 0, mostly used when needing to reset a non-looping animation
func (ac *AnimateComponent) Reset() {
	ac.currentIdx = 0
	ac.lastDisplayChange = time.Now()
}

// Update is called as a byproduct of the main game loop, this will update the animation according to the frame rate.
// Will handle non-looping animations.
func (ac *AnimateComponent) Update() error {
	if time.Since(ac.lastDisplayChange) >= time.Duration(ac.frameInterval) {
		ac.currentIdx += 1

		ac.currentIdx = ac.currentIdx % len(ac.sprites)
		if ac.currentIdx == 0 && !ac.loop {
			ac.complete()
		}

		ac.lastDisplayChange = time.Now()
	}

	return nil
}

// Draw draws the current frame to the screen. It takes the screen as well as an X and Y offset.
// These offsets are not to be confused with the Animateable offset, this offset is for a camera projection.
// Whereas the Animateable offset is used for differences in the sprites as they are drawn in their source files.
func (ac *AnimateComponent) Draw(screen *ebiten.Image, xOffset, yOffset float64) {
	currentImg := ac.sprites[ac.currentIdx]

	opts := &ebiten.DrawImageOptions{}

	// Center the image if so desired
	if ac.centerImageOnOrigin {
		width := currentImg.Bounds().Dx()
		height := currentImg.Bounds().Dy()

		opts.GeoM.Translate(-float64(width/2), -float64(height/2))
	}

	// Scale the image
	opts.GeoM.Scale(ac.subject.Scale())

	// If the character is facing the opposite horizontal direction, flip it
	if ac.subject.IsHorizontalFlipped() {
		opts.GeoM.Scale(-1, 1)
	}

	// If the character is facing the opposite vertical direction, flip it
	if ac.subject.IsVerticalFlipped() {
		opts.GeoM.Scale(1, -1)
	}
	drawXOffset, drawYOffset := ac.subject.DrawOffset()
	opts.GeoM.Translate(ac.subject.At().X()+xOffset+drawXOffset, ac.subject.At().Y()+yOffset+drawYOffset)

	screen.DrawImage(currentImg, opts)
}

// CenterImage is the fluent api for setting the image to be centered
func (ac *AnimateComponent) CenterImage() *AnimateComponent {
	ac.centerImageOnOrigin = true
	return ac
}

// DisableLoop is the fluent api for disabling the loop and setting the complete callback
func (ac *AnimateComponent) DisableLoop(complete func()) *AnimateComponent {
	ac.loop = false
	ac.complete = complete
	return ac
}

// AnimateCenterImage configures the AnimateComponent to draw with reference to the center and not the top left.
// This is used to help with flipping horizontally and vertically.
func AnimateCenterImage() AnimateOpt {
	return func(ac *AnimateComponent) {
		ac.centerImageOnOrigin = true
	}
}

// AnimateDisableLoop turns of the looping for the particular animation.
func AnimateDisableLoop(complete func()) AnimateOpt {
	return func(ac *AnimateComponent) {
		ac.loop = false
		ac.complete = complete
	}
}

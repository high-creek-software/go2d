module gitlab.com/high-creek-software/go2d

go 1.21.5

require (
	github.com/ebitengine/gomobile v0.0.0-20240429094902-cf88669c3591 // indirect
	github.com/ebitengine/hideconsole v1.0.0 // indirect
	github.com/ebitengine/purego v0.7.1 // indirect
	github.com/hajimehoshi/ebiten/v2 v2.7.3 // indirect
	github.com/jezek/xgb v1.1.1 // indirect
	github.com/quartercastle/vector v0.2.0 // indirect
	golang.org/x/exp/shiny v0.0.0-20240119083558-1b970713d09a // indirect
	golang.org/x/image v0.15.0 // indirect
	golang.org/x/mobile v0.0.0-20240112133503-c713f31d574b // indirect
	golang.org/x/sync v0.7.0 // indirect
	golang.org/x/sys v0.20.0 // indirect
)

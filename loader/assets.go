// Package loader provides an asset loading and caching implementation.
package loader

import (
	"fmt"
	"github.com/hajimehoshi/ebiten/v2"
	"gitlab.com/high-creek-software/go2d/cache"
	"image"
	_ "image/jpeg"
	_ "image/png"
	"io"
	"io/fs"
)

// AssetLoader is the struct that uses a fs.FS file system and a map for caching game assets.
type AssetLoader struct {
	dir   fs.FS
	cache *cache.Cache[string, *ebiten.Image]
}

// NewAssetLoader takes the directory (dir fs.FS) and creates a new AssetLoader
func NewAssetLoader(dir fs.FS) *AssetLoader {
	il := &AssetLoader{
		cache: cache.New[string, *ebiten.Image](),
		dir:   dir,
	}

	return il
}

// LoadImage takes the path, with relation to the root directory of the embeded fs.FS, and returns an *ebiten.Image, error
//
// path -> complete path of the asset rooted at the fs.FS
//
// *ebiten.Image, error
func (al *AssetLoader) LoadImage(path string) (*ebiten.Image, error) {
	var cachedImage *ebiten.Image
	var ok bool

	cachedImage, ok = al.cache.Get(path)

	if ok {
		return cachedImage, nil
	}

	r, err := al.dir.Open(path)
	if err != nil {
		return nil, err
	}
	defer r.Close()

	img, _, err := image.Decode(r)
	if err != nil {
		return nil, err
	}

	eImg := ebiten.NewImageFromImage(img)

	al.cache.Set(path, eImg)

	return eImg, nil
}

// LoadSubImage takes a root image and a rectangle for that image and returns an image for that rectangle
func (al *AssetLoader) LoadSubImage(rootPath string, rect image.Rectangle) (*ebiten.Image, error) {
	subImageKey := fmt.Sprintf("%s:%s", rootPath, rect.String())

	subImg, ok := al.cache.Get(subImageKey)
	if ok {
		return subImg, nil
	}

	img, err := al.LoadImage(rootPath)
	if err != nil {
		return nil, err
	}

	rectImg := img.SubImage(rect)
	subImg = ebiten.NewImageFromImage(rectImg)
	al.cache.Set(subImageKey, subImg)

	return subImg, nil
}

// MustLoadSubImage just like LoadSubImage but panics if there is an error
func (al *AssetLoader) MustLoadSubImage(rootPath string, rect image.Rectangle) *ebiten.Image {
	img, err := al.LoadSubImage(rootPath, rect)
	if err != nil {
		panic(fmt.Errorf("error loading submimage: %s - %s -> %v", rootPath, rect.String(), err))
	}

	return img
}

// LoadContiguousSubImages will extract individual frames from a parent image and return them as an array of *ebiten.Image
func (al *AssetLoader) LoadContiguousSubImages(width, height, cols int, rootPath string) ([]*ebiten.Image, error) {

	var imgs []*ebiten.Image
	for i := 0; i < cols; i++ {
		topLeftX := i * width
		topLeftY := 0
		bottomRightX := topLeftX + width
		bottomRightY := topLeftY + height

		rect := image.Rect(topLeftX, topLeftY, bottomRightX, bottomRightY)

		subImg := al.MustLoadSubImage(rootPath, rect)
		imgs = append(imgs, subImg)

	}

	return imgs, nil
}

// LoadConcurrentDirectory takes a path (with relation to the root directory of the fs.FS), an asset file extension, and how many items exist in the directory
//
// path -> goes to a directory of index named files 0.png, 1.png, 2.png
// extension -> the extension of the files
// total -> how many files exist in the directory
//
// []*ebiten.Image
func (al *AssetLoader) LoadConcurrentDirectory(path, extension string, total int) []*ebiten.Image {
	var res []*ebiten.Image
	for i := 0; i < total; i++ {
		if eImg, err := al.LoadImage(fmt.Sprintf("%s/%d.%s", path, i, extension)); err == nil {
			res = append(res, eImg)
		}
	}
	return res
}

// MustLoadImage takes a path (with relation to the root directory of the asset file), will panic if the file doesn't exist
//
// path -> complete path of the asset rooted at the fs.FS
//
// *ebiten.Image
func (al *AssetLoader) MustLoadImage(path string) *ebiten.Image {
	img, err := al.LoadImage(path)

	if err != nil {
		panic(fmt.Errorf("error loading image: %s -> %v", path, err))
	}

	return img
}

// GetReader takes a path (with relation to the root directory of the asset file), returns a io.ReadCloser to interact with
//
// path -> complete path of the asset rooted at the fs.FS
//
// io.ReadCloser, error
func (al *AssetLoader) GetReader(path string) (io.ReadCloser, error) {

	r, err := al.dir.Open(path)
	if err != nil {
		return nil, err
	}

	return r, nil
}

// MustGetReader takes a path (with relation to the root directory of the asset file), returns a io.ReadCloser to interact with, will panic if the file doesn't exist
//
// path -> complete path of the asset rooted at the fs.FS
//
// io.ReadCloser, error
func (al *AssetLoader) MustGetReader(path string) io.ReadCloser {
	r, err := al.GetReader(path)

	if err != nil {
		panic(fmt.Errorf("error loading reader: %s -> %v", path, err))
	}

	return r
}

package collision

const (
	default_max_objects = 10
	default_max_levels  = 5
)

// Quadtree is a wrapper for quickly looking up possible collisions for it's element type R
// Because collision calculations are expensive to run, the quadtree looks for elements that are in the same quadrant
// to return a slice of possible collisions. Where further collision detection algorithms can be run.
type Quadtree[R Rectangler] struct {
	maxObjects, maxLevels int

	level   int
	objects []R
	bounds  Rect
	nodes   [4]*Quadtree[R]
}

// NewQuadTree creates a new QuadTree with element type R, number of levels, and the bounds of the 2D plane
func NewQuadtree[R Rectangler](bounds Rect) *Quadtree[R] {
	qt := &Quadtree[R]{
		maxObjects: default_max_objects,
		maxLevels:  default_max_levels,
		level:      0,
		bounds:     bounds,
	}

	return qt
}

func newQuadTreeWithLevel[R Rectangler](level int, bounds Rect) *Quadtree[R] {
	qt := &Quadtree[R]{
		maxObjects: default_max_objects,
		maxLevels:  default_max_levels,
		level:      level,
		bounds:     bounds,
	}

	return qt
}

// Clear removes all elements from the QuadTree
func (qt *Quadtree[R]) Clear() {
	qt.objects = nil

	for idx, n := range qt.nodes {
		if n != nil {
			n.Clear()
			qt.nodes[idx] = nil
		}
	}
}

func (qt *Quadtree[R]) split() {
	subWidth := int(qt.bounds.Width / 2)
	subHeight := int(qt.bounds.Height / 2)

	x := int(qt.bounds.Origin.X())
	y := int(qt.bounds.Origin.Y())

	qt.nodes[0] = newQuadTreeWithLevel[R](qt.level+1, NewRect(float64(x+subWidth), float64(y), float64(subWidth), float64(subHeight)))
	qt.nodes[1] = newQuadTreeWithLevel[R](qt.level+1, NewRect(float64(x), float64(y), float64(subWidth), float64(subHeight)))
	qt.nodes[2] = newQuadTreeWithLevel[R](qt.level+1, NewRect(float64(x), float64(y+subHeight), float64(subWidth), float64(subHeight)))
	qt.nodes[3] = newQuadTreeWithLevel[R](qt.level+1, NewRect(float64(x+subWidth), float64(y+subHeight), float64(subWidth), float64(subHeight)))
}

func (qt *Quadtree[R]) getIndex(rect Rect) int {
	index := -1
	verticalMidpoint := qt.bounds.Origin.X() + (qt.bounds.Width / 2)
	horizontalMidpoint := qt.bounds.Origin.Y() + (qt.bounds.Height / 2)

	topQuadrant := rect.Origin.Y() < horizontalMidpoint && rect.Origin.Y()+rect.Height < horizontalMidpoint
	bottomQuadrant := rect.Origin.Y() > horizontalMidpoint

	if rect.Origin.X() < verticalMidpoint && rect.Origin.X()+rect.Width < verticalMidpoint {
		if topQuadrant {
			index = 1
		} else if bottomQuadrant {
			index = 2
		}
	} else if rect.Origin.X() > verticalMidpoint {
		if topQuadrant {
			index = 0
		} else if bottomQuadrant {
			index = 3
		}
	}

	return index
}

// Insert adds an element R to the internal objects slice.
// Checks to determine if a new quadrant needs to be formed.
func (qt *Quadtree[R]) Insert(rect R) {
	if qt.nodes[0] != nil {
		if index := qt.getIndex(rect.Rect()); index != -1 {
			qt.nodes[index].Insert(rect)

			return
		}
	}

	qt.objects = append(qt.objects, rect)
	if len(qt.objects) > qt.maxObjects && qt.level < qt.maxLevels {
		if qt.nodes[0] == nil {
			qt.split()
		}

		idx := 0
		treeIndex := 0
		for idx < len(qt.objects) {
			treeIndex = qt.getIndex(qt.objects[idx].Rect())
			if treeIndex != -1 {
				splice := qt.objects[idx]
				qt.objects = append(qt.objects[:idx], qt.objects[idx+1:]...)
				qt.nodes[treeIndex].Insert(splice)
			} else {
				idx++
			}
		}
	}
}

// Retrieve takes a Rectangler (not necessarily of type R of the QuadTree) and returns a slice of objects in the same quadrant
func (qt *Quadtree[R]) Retrieve(rect Rectangler) []R {
	results := qt.objects
	if qt.nodes[0] != nil {

		if idx := qt.getIndex(rect.Rect()); idx != -1 {
			if res := qt.nodes[idx].Retrieve(rect); res != nil {
				results = append(results, res...)
			}
		} else {
			// I've found a case where the rect subject, can straddle two quadrants and then get no index to look into, I need a better lookup for the subject indecies
			// This just starts loading all of the child nodes Rs
			for i := 0; i < len(qt.nodes); i++ {
				results = append(results, qt.nodes[i].Retrieve(rect)...)
			}
		}
	}

	//results = append(results, qt.objects...)

	return results
}

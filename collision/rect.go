package collision

import (
	"math"

	"github.com/quartercastle/vector"
)

// Rectangler is an interface that provides a Rect
type Rectangler interface {
	Rect() Rect
}

// Rect represents a 2d rectangle at Origin with Width and Height
type Rect struct {
	Origin        vector.Vector
	Width, Height float64
}

// NewRect creates a rectangle at x, y with given width and height
func NewRect(x, y, width, height float64) Rect {
	origin := vector.Vector{x, y}

	return Rect{
		Origin: origin,
		Width:  width,
		Height: height,
	}
}

// AlignedCollides checks for collision between two Rects, that are aligned (i.e. neither of them rotated)
func (r Rect) AlignedCollides(r2 Rect) bool {
	return r.Origin.X() < r2.Origin.X()+r2.Width &&
		r.Origin.X()+r.Width > r2.Origin.X() &&
		r.Origin.Y() < r2.Origin.Y()+r2.Height &&
		r.Origin.Y()+r.Height > r2.Origin.Y()
}

// CollidesWithCircle checks a rectangle and a circle for collision
func (r Rect) CollidesWithCircle(c Circle) bool {
	return checkCircleRectCollision(c, r)
}

func checkCircleRectCollision(c Circle, r Rect) bool {
	testX := c.Center.X()
	testY := c.Center.Y()

	if c.Center.X() < r.Origin.X() {
		testX = r.Origin.X()
	} else if c.Center.X() > r.Origin.X()+r.Width {
		testX = r.Origin.X() + r.Width
	}

	if c.Center.Y() < r.Origin.Y() {
		testY = r.Origin.Y()
	} else if c.Center.Y() > r.Origin.Y()+r.Height {
		testY = r.Origin.Y() + r.Height
	}

	distX := c.Center.X() - testX
	distY := c.Center.Y() - testY
	distance := math.Sqrt((distX * distX) + (distY * distY))

	return distance <= c.Radius
}

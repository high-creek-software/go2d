package collision

import (
	"math"

	"github.com/quartercastle/vector"
)

// Circle represents a 2d circle at Center with Radius
type Circle struct {
	Center vector.Vector
	Radius float64
}

// NewCircle creates a new circle at point x, y and radius
func NewCircle(x, y, radius float64) Circle {
	return Circle{
		Center: vector.Vector{x, y},
		Radius: radius,
	}
}

// Collides compares two circles to check for collision
func (c Circle) Collides(c2 Circle) bool {
	dist := math.Sqrt(math.Pow(c2.Center.X()-c.Center.X(), 2) + math.Pow(c2.Center.Y()-c.Center.Y(), 2))
	return dist <= c.Radius+c2.Radius
}

// CollidesWithRect compares a circle and a rect for collision
func (c Circle) CollidesWithRect(r Rect) bool {
	return checkCircleRectCollision(c, r)
}

# go2d
---

## A basic tool set for starting game development with the ebitengine go game engine
The ebitengine game engine is a great engine for what it does. However, there are a handful of other features that it would be great to have available. That is what this basic 2d bolt on intends to provide.

- Basic collision detection
- Basic components
- Basic finite state machine implementation
- Basic file loader

## Installation
go get -u gitlab.com/high-creek-software/go2d@latest
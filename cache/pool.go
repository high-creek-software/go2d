package cache

// Pool provides a channel based fifo pool of game objects
type Pool[T any] struct {
	pool    chan T
	factory func() T
}

// NewPool creates a new pool of size max, with a factory method
func NewPool[T any](max int, factory func() T) *Pool[T] {
	return &Pool[T]{
		pool:    make(chan T, max),
		factory: factory,
	}
}

// Borrow return the first item, or a new item
func (p *Pool[T]) Borrow() T {
	var t T
	select {
	case t = <-p.pool:
	default:
		t = p.factory()
	}

	return t
}

// Return returns an item to the pool
func (p *Pool[T]) Return(t T) {
	select {
	case p.pool <- t:
	default:
	}
}

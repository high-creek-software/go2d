package cache

import "sync"

// Cache is a basic cache without eviction
type Cache[K comparable, V any] struct {
	data  map[K]V
	mutex sync.RWMutex
}

// New returns a new cache
func New[K comparable, V any]() *Cache[K, V] {
	return &Cache[K, V]{
		data: make(map[K]V),
	}
}

// Set adds a value by key to the cache
func (c *Cache[K, V]) Set(key K, val V) {
	c.mutex.Lock()
	defer c.mutex.Unlock()

	c.data[key] = val
}

// Get looks for the value of key
func (c *Cache[K, V]) Get(key K) (V, bool) {
	c.mutex.RLock()
	defer c.mutex.RUnlock()

	val, ok := c.data[key]
	return val, ok
}

// Remove removes a value from the cache
func (c *Cache[K, V]) Remove(key K) {
	c.mutex.Lock()
	defer c.mutex.Unlock()

	delete(c.data, key)
}
